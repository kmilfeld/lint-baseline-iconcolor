# lint-baseline-iconcolor

Lint Baseline For IconColors Warning Doesn't Work For Icons With Multiple Densities In Gradle Plugin version 3.2.0


Steps to repro:
1) Delete .app/lint-baseline.xml
2) Run `./gradlew lint` to create new baseline file (it should match the one checked in)
3) Rerun `./gradlew lint`
4) Notice it errors out with the following error:
`lint-baseline-iconcolor/app/src/main/res/drawable-xhdpi/ic_action_up_blue.png: Error: Action Bar icons should use a single gray color (#333333 for light themes (with 60%/30% opacity for enabled/disabled), and #FFFFFF with opacity 80%/30% for dark themes [IconColors]`
5) Checkout branch `manually-updated-baseline-file`
6) Rerun steps 2 & 3
7) Notice the lack of an error

The lint-baseline.xml file had to be manually updated in order for lint to ignore the IconColors warning correctly.